const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema(
    {
        userId: {
            type: String,
            required: true,
            ref: "User"
        },
        name: {
            type: String,
            ref: "Product"
        },
        quantity: {
            type: Number,
            default: 1
        },
        amount: {
            type: Number,
            required: true
        },
        subTotal: {
            type: Number,
            required: true
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model("Order", orderSchema)