const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//CHECK EMAIL
module.exports.checkEmailExist = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0){
            return true;
        }
        else {
            return false;
        }
    })
};

//REGISTER
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false;
		} else {
			return true
		}
	});
};

//LOGIN USER
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {
		//User does not exist
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
                
			} else {
				return false;
			}
		};
	});
};

//GET USER DETAILS
module.exports.getDetails = (data) => {
	return User.findById(data).then(result => {
		result.password = "";
		return result;
	})
}




