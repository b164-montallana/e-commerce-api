const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

//ADD PRODUCT
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product ({
        name: reqBody.name,
        description: reqBody.description,
        categories: reqBody.categories,
        price: reqBody.price,
        stock: reqBody.stock,
        isActive: reqBody.isActive
    })
    return newProduct.save().then((product, error) => {
        if(error){
            return false;
        } else {
            return true;
        }
    })
}

//GET ALL ACTIVE
module.exports.getAllActive = () => {
    return Product.find({ isActive: true }).then(result => {
        return result;
    })
}

//GET ALL
module.exports.getAll = () => {
    return Product.find({}).then(result => {
        return result;
    })
}

//GET PRODUCT BY ID
module.exports.getOne = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result;
    })
}

//UPDATE A PRODUCT
module.exports.updateProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        categories: reqBody.categories,
        price: reqBody.price,
        stock: reqBody.stock,
        isActive: reqBody.isActive
    };
    return Product.findByIdAndUpdate(reqParams, updatedProduct).then((product, error) => {
        if(error){
            return false;
        } else {
            return true;
        }
    })
}

//DELETE A PRODUCT
module.exports.deleteProduct = (reqParams) => {
    return Product.findByIdAndDelete(reqParams).then(result => {
        return true;
    })
}


//Archive products
module.exports.archiveProduct = (reqParams) => {
    let updateActiveField = {
        isActive : false
    }
    return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error)=> {
        if(error){
            return false;
        } else {
            return true;
        }
    })
}


//Activate products
module.exports.activateProduct = (reqParams) => {
    let updateActiveField = {
        isActive : true
    }
    return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error)=> {
        if(error){
            return false;
        } else {
            return true;
        }
    })
}